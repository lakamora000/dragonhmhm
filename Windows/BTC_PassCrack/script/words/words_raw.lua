
require('base')

local raw={
	init = nil,
	at = nil,
	eof = nil,
	total = nil,
	hash = nil,
}

function raw:init(param)
	if type(param)=='string' then
		self.keys = string.split(param, '|')
	elseif type(param)=='table' then
		self.keys = param
	else
		assert(false, "invalid param "..json_encode(param))
		self.keys = param
	end
end

function raw:at(id)
	assert(self.keys[id])
	return self.keys[id]
end

function raw:total()
	assert(self.keys)
	local minlen = nil
	for k,v in pairs(self.keys) do
		local len = v:len()
		if not minlen or len<minlen then
			minlen = len
		end
		if minlen==1 then
			break
		end
	end
	minlen = minlen or 1
	return #self.keys, minlen
end
function raw:hash()
	assert(self.keys)
	local str = ''
	for k,v in pairs(self.keys) do
		str = str .. v .. '|'
	end
	return md5(str)
end
return raw
