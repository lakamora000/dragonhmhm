local split={}
require('base')
local conf_surplus_size = 0

local tmpl_library = {
-- [len] = { [word]=true, ... }
}

assert(LST_ROOT)

function _init()
	local files = {
		"pinyin.lst",
		'english.lst',
	}
	for k,file in pairs(files)do
		local path = LST_ROOT..file
		local fp = io.open(path, 'r')
		assert(fp, 'open fail ' .. path)
		for line in fp:lines() do
			local slen = line:len()
			if slen>1 and line[1]~='#' then
				if not tmpl_library[slen] then
					tmpl_library[slen]={}
				end
				tmpl_library[slen][line] = true
			end
		end
		fp:close()
	end
end

local _is_init = false
function split.split(src_line, add)
	if not _is_init then
		_is_init = true
		_init()
	end
	--if true then return end

	src_line = src_line:gsub("[^a-zA-Z]+", "")
	if src_line:len()<=0 then
		return
	end
	
	local eof = false
	local rs_stack = { {str=src_line,keys={}, ignoreSize=1} }
	
	while #rs_stack > 0 do
		local rs_curr = rs_stack[#rs_stack]
		table.remove(rs_stack, #rs_stack)

		for wordLen, map in pairs(tmpl_library) do
			if rs_curr.str:len()>=wordLen then
				if rs_curr.ignoreSize<0 then
					rs_curr.ignoreSize=0
				end
				for i=1,rs_curr.ignoreSize+1 do
					local left = rs_curr.str:sub(i, i+wordLen-1)
					if left and map[left:lower()] then
						local newstr=rs_curr.str:sub(i+wordLen)
						local rs_new = { 
							str=newstr, 
							keys=table.clone(rs_curr.keys), 
							ignoreSize=rs_curr.ignoreSize-i+1,
						}
						table.insert(rs_new.keys, left)
						
						if( rs_new.str:len() > conf_surplus_size ) then
							table.insert(rs_stack, rs_new)
						else
							for k,v in pairs(rs_new.keys)do
								add(v)
							end
							if rs_new.str:len() then
								add(rs_new.str)
							end

						end
					end
				end
			end

		end
	end
	
end



return split;