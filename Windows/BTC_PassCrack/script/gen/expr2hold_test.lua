local expr_to_holder = require ('expr2hold').expr_to_holder
------------------------------------------------------------------------------
local function test_expr()
    local MAX_COUNT = 88

	local ret
	ret = expr_to_holder("fuck[abcd]")
	assert(table.deep_equal(ret, {
		{count={min=1,max=1}, select={ {keys={'fuck'}}, },},
		{count={min=1,max=1}, select={ {keys={'a','b','c','d',}}, },},
	}))
	ret = expr_to_holder("fuck[ab][cd]*[ef]+[gh]?[ij]{1,4}[kl]{2,}[mn]{1}")
	assert(table.deep_equal(ret, {
		{count={min=1,max=1}, 			select={ {keys={'fuck'}}, },},
		{count={min=1,max=1}, 			select={ {keys={'a','b',}}, },},
		{count={min=0,max=MAX_COUNT}, 	select={ {keys={'c','d',}}, },},
		{count={min=1,max=MAX_COUNT}, 	select={ {keys={'e','f',}}, },},
		{count={min=0,max=1}, 			select={ {keys={'g','h',}}, },},
		{count={min=1,max=4}, 			select={ {keys={'i','j',}}, },},
		{count={min=2,max=MAX_COUNT}, 	select={ {keys={'k','l',}}, },},
		{count={min=1,max=1}, 			select={ {keys={'m','n',}}, },},
	}))

	ret = expr_to_holder("fuck[a-d]")
	assert(table.deep_equal(ret, {
		{count={min=1,max=1}, select={ {keys={'fuck'}}, },},
		{count={min=1,max=1}, select={ {keys={'a','b','c','d',}}, },},
	}))
	ret = expr_to_holder("fuck[aba-dbcd]")
	assert(table.deep_equal(ret, {
		{count={min=1,max=1}, select={ {keys={'fuck'}}, },},
		{count={min=1,max=1}, select={ {keys={'a','b','c','d',}}, },},
	}))

	ret = expr_to_holder([[fu\\ck\[abcd]+test2[a\]b]+]])
	assert(table.deep_equal(ret, {
		{count={min=1,max=1}, select={ {keys={[[fu\ck[abcd]+test2]]}}, },},
		{count={min=1,max=MAX_COUNT}, select={ {keys={'a',']','b',}}, },},
	}))

	ret = expr_to_holder([[fuck[a\-d][h\\-g][0\\\-9][+-*/]+]])
	assert(table.deep_equal(ret, {
		{count={min=1,max=1}, 			select={ {keys={'fuck'} },},},
		{count={min=1,max=1}, 			select={ {keys={'a','-','d'}}, },},
		{count={min=1,max=1}, 			select={ {keys={'h','\\',']','^','_','`','a','b','c','d','e','f','g'}}, },},
		{count={min=1,max=1}, 			select={ {keys={'0','\\','-','9',}}, },},
		{count={min=1,max=MAX_COUNT}, 	select={ {keys={'/',}}, },},
	}))
	
	ret = expr_to_holder("[{file:password.lst}]+")
	assert(table.deep_equal(ret, {
		{count={min=1,max=MAX_COUNT}, select={ {from='file', param='password.lst',}, },},
	}))
	ret = expr_to_holder("[{file:password.lst}]*[{file:pinyin.lst}]*")
	assert(table.deep_equal(ret, {
		{count={min=0,max=MAX_COUNT}, select={ {from='file', param='password.lst',}, },},
		{count={min=0,max=MAX_COUNT}, select={ {from='file', param='pinyin.lst',}, },},
	}))
	ret = expr_to_holder("[{file:password.lst}a-d]+")
	assert(table.deep_equal(ret, {
		{count={min=1,max=MAX_COUNT}, select={ {keys={'a','b','c','d',}},{from='file', param='password.lst',}, },},
	}))
	ret = expr_to_holder("[{file:password.lst}a-zA-Z0-9]+")
	ret = expr_to_holder("[{file:password.lst}{file:pinyin.lst}a-zA-Z0-9]+")
	ret = expr_to_holder("[{file:a.lst}][{file:b.lst}{netbb}][a-zA-Z0-9]+")

	ret = expr_to_holder("[{file:password.lst}{http:http://127.0.0.1/gen_query.php}a-zA-Z0-9]+")

    print('test finish')
end
if TEST_MODE or TEST_TARGET=='expr2hold' then
	test_expr()
end