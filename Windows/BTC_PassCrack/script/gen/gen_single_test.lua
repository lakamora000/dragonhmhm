local gen = require('gen_single')

------------------------------------------------------------------------------
local function test_gen(opt)
	local ct = os.clock()
	local ctx={}
	local obj={}
	gen.init(obj, opt, ctx)
    local maxtime = 15
    local total = 0
    local start = os.time()
	while true do
		local str = gen.next(obj, ctx)
        if not str then
			break
        end
        total = total + 1
        --print(str)
        if os.time() - start > maxtime then
            break
        end
	end
    ct = os.clock() - ct
    local speed = total / ct
	print("taketime="..ct..",total="..total..", speed="..speed)
end

if TEST_MODE or TEST_TARGET=='single'  then
    test_gen( {module='raw', module_param='0|1|2|3|4|5|6|7|8|9', } )
    test_gen( {module='file', module_param='age.lst', } )
    --test_gen( {module='http', module_param='http://127.0.0.1/genkeys/', } )
    print('test finish')
end

return gen