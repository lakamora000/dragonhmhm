require('base')


local function idlist_next2(idlist, maxlen, minval, maxval_tbl)
	local i = 1
	while i <= maxlen do
		idlist[i] = (idlist[i] or (minval-1))+1
		if idlist[i] > maxval_tbl[i] then
			idlist[i] = minval
			i = i + 1
		else
			return true, i
		end
	end
	return false
end
local function idlist_next3(idlist, maxlen, minval_tbl, maxval_tbl)
	local i = 1
	while i <= maxlen do
		idlist[i] = (idlist[i] or (minval_tbl[i]-1))+1
		if idlist[i] > maxval_tbl[i] then
			idlist[i] = minval_tbl[i]
			i = i + 1
		else
			return true, i
		end
	end
	return false
end
------------------------------------------------------------------------------
local CountGen={}

function CountGen:init(info, ctx)
	self.info = info

	local MaxId = nil
	local MaxMinId = nil
	local newMinTbl = {}
	for i,v in pairs(info) do
		
		if not MaxId or MaxId < v.max then
			MaxId = v.max
		end
		if not MaxMinId or MaxMinId < v.min then
			MaxMinId = v.min
		end
		table.insert(newMinTbl, v.min)
	end
	self.MaxId = MaxId
	self.newMinTbl = newMinTbl

	if not ctx.nowCount then
		ctx.nowCount = MaxMinId
	end
	local sub1 = false
	if not self.firstMax or not self.newMaxTbl then
		CountGen.genMaxCount(self, ctx.nowCount)
		sub1 = true
	end

	if not ctx.idlist then
		ctx.idlist = {}
		for i, v in pairs(info) do
			ctx.idlist[i] = v.min
		end
		sub1 = true
	end
	if sub1 then
		ctx.idlist[1] = ctx.idlist[1] - 1
	end
end
----
function CountGen:genMaxCount(nowCount)
	local firstMax = nil
	local MaxCount = nil
	local newMaxTbl = {}
	for i=1, #self.info do
		newMaxTbl[i] = math.min(self.info[i].max, nowCount)
		assert(newMaxTbl[i] >= self.info[i].min)
		if not firstMax or newMaxTbl[i]>MaxCount  then
			firstMax = i
			MaxCount = newMaxTbl[i]
		end
		newMaxTbl[i] = math.max(newMaxTbl[i], self.info[i].min)
	end
	self.firstMax = firstMax
	self.newMaxTbl = newMaxTbl
end
function CountGen:next(ctx)
	local length = #self.info
	if ctx.nowCount > self.MaxId then
		return false
	end
	while true do
		assert(self.newMaxTbl)
		assert(self.firstMax)
		local next_ok,step = idlist_next3(ctx.idlist, length, self.newMinTbl, self.newMaxTbl)
		if next_ok then
			if step > 1 then
				local underflow = 0
				for _,id in pairs(ctx.idlist) do
					if id < ctx.nowCount then
						underflow = underflow + 1
					else
						break;
					end
				end
				if underflow == length then
					--print('underflow', underflow, nowCount, table.dump(ids2raw(ctx.idlist)), self.firstMax)
					ctx.idlist[self.firstMax] = self.newMaxTbl[self.firstMax]
				end
			end
			break
		end
		ctx.nowCount = ctx.nowCount + 1
		if ctx.nowCount > self.MaxId then
			return false
		end
		CountGen.genMaxCount(self, ctx.nowCount)
		ctx.idlist = {}
		for i=1,length do
			ctx.idlist[i] = self.info[i].min
		end
		assert(self.firstMax);
		ctx.idlist[self.firstMax] = self.newMaxTbl[self.firstMax]
		ctx.idlist[1] = ctx.idlist[1] - 1
	end

	local ret={}
	for i,id in pairs(ctx.idlist) do
		ret[i] = id
	end

	return ret
end

--[[
function CountGen:init(info, ctx)
	self.rawinfo = info

	local sorted = {}
	for i,v in pairs(info) do
		sorted[i] = table.clone(v)
		sorted[i].oldi = i
	end
	table.sort(sorted, function(a,b)
		if a.min == b.min then
			return a.max < b.max
		end
		return a.min < b.min
	end)
	local sorted2raw = {}
	for i,v in pairs(sorted) do
		sorted2raw[i] = info[sorted[i].oldi]
	end
	self.sorted2raw = sorted2raw

	local cilist = {}
	for _,v in pairs(sorted) do
		table.insert(cilist, v.max-v.min+1)
	end

	local MaxCount = 0
	for _,count in pairs(cilist) do
		if count > MaxCount then
			MaxCount = count
		end
	end
	self.cilist = cilist
	self.MaxCount = MaxCount
end
function CountGen:next(ctx)
	local length = #self.cilist

	if ctx.next_ok then
		assert(self.newMaxTbl)
		assert(self.firstMax)
		local step
		ctx.next_ok,step = idlist_next2(ctx.idlist, length, 0, self.newMaxTbl)
		if ctx.next_ok then
			if step > 1 then
				local underflow = 0
				for _,id in pairs(ctx.idlist) do
					if id<ctx.nowCount then
						underflow = underflow + 1
					else
						break
					end
				end
				if underflow == length then
					--print('underflow', underflow, nowCount, table.dump(ids2raw(ctx.idlist)), self.firstMax)
					ctx.idlist[self.firstMax] = ctx.nowCount
				end
			end
		end
	end
	
	if not ctx.next_ok then
		local nowCount = (ctx.nowCount or -1)
		nowCount = nowCount+1
		if nowCount > self.MaxCount-1 then
			return false
		end
		
		local newMaxTbl = {}
		local firstMax = nil
		for i=1, length do
			newMaxTbl[i] = math.min(self.cilist[i]-1, nowCount)
			if not firstMax and newMaxTbl[i] >= nowCount then
				firstMax = i
			end
		end
		self.firstMax = firstMax
		self.newMaxTbl = newMaxTbl
	
		ctx.idlist = {}
		for i=1,length do
			ctx.idlist[i] = 0
		end
		assert(firstMax);
		ctx.idlist[firstMax] = nowCount
		
		ctx.nowCount = nowCount
		ctx.next_ok = true
			
	end

	local ret={}
	for i,id in pairs(ctx.idlist) do
		ret[i] = id + self.sorted2raw[i].min
	end

	return ret
end
]]
------------------------------------------------------------------------------
return CountGen
